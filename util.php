<?php

//function for creating the connection with the DB
function createConnection()
{
    $mysql = mysqli_connect("localhost","root","","mvc");
    return $mysql;
}

//closing connection
function closeConnection($mysql)
{
    mysqli_close($mysql);
}


//function for DML
function dml($sql){
    //open connection
    $mysql=createConnection();

    //Check if the dml was correct
    if ($mysql->query($sql) === TRUE) {

        return TRUE;

    } else {

        return FALSE;
    }

    //closing connection
    closeConnection($mysql);

}



